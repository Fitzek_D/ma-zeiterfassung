﻿using System;
namespace MA_Zeiterfassung.Dto
{
    public class ArbeiterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Stunden { get; set; }
    }
}
