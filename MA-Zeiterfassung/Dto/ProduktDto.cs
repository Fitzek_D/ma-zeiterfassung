﻿using System;
namespace MA_Zeiterfassung.Dto
{
    public class ProduktDto
    {
        public string Produktname { get; set; }
        public string Produktlink { get; set; }
        public string ProduktIconLink { get; set; }
    }

}
