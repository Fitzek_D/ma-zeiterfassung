export interface ArbeiterCreateDto {
    name: string,
    email: string,
    tel: string,
    gehalt: number,
    userId: number
}