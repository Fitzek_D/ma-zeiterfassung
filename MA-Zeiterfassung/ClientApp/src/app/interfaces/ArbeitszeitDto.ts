export interface ArbeitszeitDto {
    arbeiterId: number,
    stundenToAdd: number,
}