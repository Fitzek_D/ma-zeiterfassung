import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { AccountIdService } from "../account-id-service";
import { AccountDto } from "../interfaces/AccountDto";
import { ArbeiterCreateDto } from "../interfaces/ArbeiterCreateDto";
import { ArbeiterDto } from "../interfaces/ArbeiterDto";
import { ArbeitszeitDto } from "../interfaces/ArbeitszeitDto";
import { AuftragDto } from "../interfaces/AuftragDto";

@Component({
    selector: 'arbeiter-app',
    templateUrl: './arbeiter.component.html',
    styleUrls: ['./arbeiter.component.css']
  })
  
  export class ArbeiterComponent implements OnInit {
    id: number;

    arbeiterId: number

    arbeiter$: Observable<ArbeiterDto> = new Observable<ArbeiterDto>();

    anmeldeForm: FormGroup;

  
    isValid = true;
  
    constructor(
        private http: HttpClient, 
        private route: ActivatedRoute, 
        private accauntService: AccountIdService, 
        private formBuilder: FormBuilder) {}
  
    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.arbeiterId = params['id'];
          });

          console.log('aus den params geholte id: ' + this.arbeiterId)
        this.loadData();

        this.accauntService.userId.subscribe(id => {
            this.id = id;
        });
        
        this.anmeldeForm = this.formBuilder.group({
            stunden: ['', Validators.required],
          });
    }

    submit() {
    
        const stunden = this.anmeldeForm.get('stunden').value;
       
        const arbeitszeit: ArbeitszeitDto = {
            arbeiterId: this.arbeiterId,
            stundenToAdd: stunden,
        };

        this.http.post('api/arbeiter/add-stunden', arbeitszeit).subscribe(response => {
            console.log(response);
            this.anmeldeForm.reset();
            this.loadData();
        })
    }

    loadData() {
        this.arbeiter$ = this.http.get<ArbeiterDto>('api/arbeiter/get-arbeiter-by-id?id=' + this.arbeiterId);
    }
   
  }
