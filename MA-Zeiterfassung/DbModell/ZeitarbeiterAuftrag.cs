﻿using System;
namespace MA_Zeiterfassung.DbModell
{
    public class ZeitarbeiterAuftrag
    {
        public int Id { get; set; }

        public Zeitarbeiter Zeitarbeiter { get; set; }
        public Auftrag Auftrag { get; set; }
    }
}
